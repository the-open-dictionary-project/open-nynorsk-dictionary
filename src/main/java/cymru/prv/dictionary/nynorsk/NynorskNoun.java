package cymru.prv.dictionary.nynorsk;


import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.GrammaticalGender;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;


/**
 * Used to represent a Nynorsk noun
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class NynorskNoun extends Word {

    private static final String SING_DEF = "singDefinite";
    private static final String PLUR_INDEF = "plurIndefinite";
    private static final String PLUR_DEF = "plurDefinite";

    private static final String GENDER = "gender";

    private final Set<GrammaticalGender> genders;
    private final String stem;

    private final List<String> singDefinite;
    private final List<String> plurIndefinite;
    private final List<String> plurDefinite;

    public NynorskNoun(JSONObject obj) {
        super(obj, WordType.noun);

        // If the word ends with either a or e we need to remove it
        stem = obj.has("stem")
                ? obj.getString("stem")
                : getNormalForm().replaceAll("[ea]$", "");

        // Fetch genders from string if it exist
        // Required for
        genders = obj.has(GENDER)
                ? GrammaticalGender.fromString(obj.getString(GENDER))
                : null;

        // fetch or auto-generate inflections
        singDefinite = Json.getStringListOrDefault(obj, SING_DEF, this::getDefaultSingDefinite);
        plurIndefinite = Json.getStringListOrDefault(obj, PLUR_INDEF, this::getDefaultPlurIndefinite);
        plurDefinite = Json.getStringListOrDefault(obj, PLUR_DEF, this::getDefaultPlurDefinite);
    }


    /**
     * Generates the set of default inflections
     * for the different nouns. If a word has
     * several genders it will generate all options.
     *
     * @return A list of definite singular version
     *         of the word.
     */
    private List<String> getDefaultSingDefinite(){
        if(genders == null)
            return null;
        List<String> list = new LinkedList<>();
        if (genders.contains(GrammaticalGender.MASCULINE))
            list.add(getNormalForm() + "en");
        if (genders.contains(GrammaticalGender.FEMININE))
            list.add(stem + "a");
        if (genders.contains(GrammaticalGender.NEUTER))
            list.add(getNormalForm() + "et");
        return list;
    }

    private List<String> getDefaultPlurIndefinite(){
        if(genders == null)
            return null;
        List<String> list = new LinkedList<>();
        if(genders.contains(GrammaticalGender.MASCULINE))
            list.add(getNormalForm() + "ar");
        if (genders.contains(GrammaticalGender.FEMININE)) {
            if(getNormalForm().endsWith("ing"))
                list.add(getNormalForm() + "ar");
            else
                list.add(stem + "er");
        }
        if(genders.contains(GrammaticalGender.NEUTER))
            list.add(getNormalForm());
        return list;
    }

    private List<String> getDefaultPlurDefinite(){
        if(genders == null)
            return null;
        List<String> list = new LinkedList<>();
        if (genders.contains(GrammaticalGender.MASCULINE))
            list.add(getNormalForm() + "ane");
        if (genders.contains(GrammaticalGender.FEMININE)) {
            if(getNormalForm().endsWith("ing"))
                list.add(getNormalForm() + "ane");
            else
                list.add(stem + "ene");
        }
        if (genders.contains(GrammaticalGender.NEUTER))
            list.add(getNormalForm() + "a");
        return list;
    }

    @Override
    protected JSONObject getInflections() {
        var obj = new JSONObject();
        obj.put(SING_DEF, new JSONArray(singDefinite));
        obj.put(PLUR_INDEF, new JSONArray(plurIndefinite));
        obj.put(PLUR_DEF, new JSONArray(plurDefinite));
        return obj;
    }

    @Override
    public JSONObject toJson() {
        var obj = super.toJson();
        if(genders != null)
            obj.put(GENDER, String.join("",
                    genders.stream().map(GrammaticalGender::toString).toArray(String[]::new)));
        return obj;
    }
}
