package cymru.prv.dictionary.nynorsk;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;


/**
 * Represents a Nynorsk dictionary
 *
 * The dictionary is not a full dictionary and is more of
 * a language pack that provides translations for other
 * dictionaries.
 *
 * This is why it's constructor requires a dictionary list
 * as it is kinda useless without it.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class NynorskDictionary extends Dictionary {

    /**
     * All the word types currently supported by the dictionary.
     * This is to ensure that words are actually imported from the
     * resources folder
     */
    private static final Map<WordType, Function<JSONObject, Word>> types = Map.of(
            WordType.noun, (w) -> new Word(w, WordType.noun),
            WordType.verb, (w) -> new Word(w, WordType.verb)
    );


    /**
     * Creates a new Nynorsk dictionary without a
     * dictionary list
     */
    public NynorskDictionary(){
        this(new DictionaryList());
    }


    /**
     * Creates a new Nynorsk dictionary.
     * For the most part this means adding translations
     * to the dictionary list
     *
     * @param list the list to add the translations to
     */
    public NynorskDictionary(DictionaryList list){
        super(list, "nn", types);
    }


    @Override
    public String getVersion() {
        return "1.2.0";
    }
}
