package cymru.prv.dictionary.nynorsk;

import cymru.prv.dictionary.common.DictionaryList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


/**
 * A set of tests to ensure that initialization works
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class TestNynorskDictionary {

    @Test
    public void testInitialization(){
        Assertions.assertDoesNotThrow(() -> new NynorskDictionary(new DictionaryList()));
    }

    @Test
    public void testNounsAreLoaded(){
        var dict = new NynorskDictionary(new DictionaryList());
        Assertions.assertTrue(0 < dict.getWords("kaffi").size());
    }

    @Test
    public void testVerbsAreLoaded(){
        var dict = new NynorskDictionary(new DictionaryList());
        Assertions.assertTrue(0 < dict.getWords("vera").size());
    }

    @Test
    public void ensureThatVersionNumbersAreTheSame() throws IOException {
        for(var line : Files.readAllLines(Path.of( "build.gradle"))){
            if(line.startsWith("version")){
                String version = line
                        .split("\\s+")[1]
                        .replace("'", "");
                Assertions.assertEquals(version, new NynorskDictionary().getVersion());
                return;
            }
        }
        Assertions.fail("Version number not found in build.gradle");
    }

}
