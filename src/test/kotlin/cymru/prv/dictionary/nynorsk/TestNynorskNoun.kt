package cymru.prv.dictionary.nynorsk

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.GrammaticalGender
import cymru.prv.dictionary.common.json.Json
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

/**
 * Tests for Nynorsk verbs
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestNynorskNoun {

    @Nested
    inner class TestGrammaticalGenders {

        @Test
        fun `noun without gender should not export gender`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "test"))
            assertFalse(word.toJson().has("gender"))
        }

        @Test
        fun `noun with single gender should export that gender`(){
            val word = NynorskNoun(JSONObject()
                    .put("normalForm", "test")
                    .put("gender", GrammaticalGender.ANIMATE.toString()))
            assertEquals(GrammaticalGender.ANIMATE.toString(), word.toJson().getString("gender"))
        }

        @Test
        fun `noun with multiple genders should export those genders`(){
            val word = NynorskNoun(JSONObject()
                    .put("normalForm", "test")
                    .put("gender", "mf"))
            assertEquals("mf".alphabetized(), word.toJson().getString("gender").alphabetized())
        }

    }

    @Nested
    inner class TestMasculineWords {

        @Test
        fun `words should generate singular definite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "gut").put("gender", "m"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "singDefinite")
            assertIterableEquals(listOf("guten"), list)
        }

        @Test
        fun `words should generate plural indefinite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "gut").put("gender", "m"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "plurIndefinite")
            assertIterableEquals(listOf("gutar"), list)
        }

        @Test
        fun `words should generate plural definite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "gut").put("gender", "m"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "plurDefinite")
            assertIterableEquals(listOf("gutane"), list)
        }

    }

    @Nested
    inner class TestFeminineWords {

        @Test
        fun `words should generate singular definite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "elv").put("gender", "f"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "singDefinite")
            assertIterableEquals(listOf("elva"), list)
        }

        @Test
        fun `words should generate plural indefinite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "elv").put("gender", "f"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "plurIndefinite")
            assertIterableEquals(listOf("elver"), list)
        }

        @Test
        fun `words should generate plural definite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "elv").put("gender", "f"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "plurDefinite")
            assertIterableEquals(listOf("elvene"), list)
        }

    }

    @Nested
    inner class TestNeuterWords{

        @Test
        fun `words should generate singular definite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "tre").put("gender", "n"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "singDefinite")
            assertIterableEquals(listOf("treet"), list)
        }

        @Test
        fun `words should generate plural indefinite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "tre").put("gender", "n"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "plurIndefinite")
            assertIterableEquals(listOf("tre"), list)
        }

        @Test
        fun `words should generate plural definite form`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "tre").put("gender", "n"))
            val infl = word.toJson()
                    .getJSONObject("inflections")
            val list = Json.getStringList(infl, "plurDefinite")
            assertIterableEquals(listOf("trea"), list)
        }

    }

    @Nested
    inner class TestSpecialWords {

        @Test
        fun `feminine words ending with -ing should have different suffixes`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "regjering").put("gender", "f"))
            val infl = word.toJson().getJSONObject("inflections")
            assertIterableEquals(listOf("regjeringa"), Json.getStringList(infl, "singDefinite"))
            assertIterableEquals(listOf("regjeringar"), Json.getStringList(infl, "plurIndefinite"))
            assertIterableEquals(listOf("regjeringane"), Json.getStringList(infl, "plurDefinite"))
        }

    }

    @Nested
    inner class TestStem {

        @Test
        fun `words ending with an e should inflect correctly`(){
            val word = NynorskNoun(JSONObject().put("normalForm", "linje").put("gender", "f"))
            val infl = word.toJson().getJSONObject("inflections")
            assertIterableEquals(listOf("linja"), Json.getStringList(infl, "singDefinite"))
            assertIterableEquals(listOf("linjer"), Json.getStringList(infl, "plurIndefinite"))
            assertIterableEquals(listOf("linjene"), Json.getStringList(infl, "plurDefinite"))
        }

        @Test
        fun `should be able to override stem`(){
            val word = NynorskNoun(JSONObject()
                .put("normalForm", "test")
                .put("stem", "teststem-")
                .put("gender", "f"))
            val infl = word.toJson().getJSONObject("inflections")
            assertIterableEquals(listOf("teststem-a"), Json.getStringList(infl, "singDefinite"))
            assertIterableEquals(listOf("teststem-er"), Json.getStringList(infl, "plurIndefinite"))
            assertIterableEquals(listOf("teststem-ene"), Json.getStringList(infl, "plurDefinite"))
        }

    }

}

fun String.alphabetized() = String(toCharArray().apply { sort() })